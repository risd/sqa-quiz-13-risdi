1. clone this repo `git clone https://gitlab.com/risd/sqa-quiz-13-risdi.git`
2. install required package`npm install`
3. run test `npx wdio run ./wdio.conf.js`
4. generate report `npx allure generate allure-results --clean`
5. open report `npx allure open`