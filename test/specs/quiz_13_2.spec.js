const pageObjects = require('../pageobjects/quiz_13_2.page')

describe('Change OTP method to Email', () => {
    it('valid login should move to the dashboard', async () => {
        await browser.deleteCookies();
        await pageObjects.open()
        await pageObjects.validLogin('AKK0000', 'Akuntes1')
        await expect(pageObjects.welcomeMessage).toBeDisplayed()
        await expect(pageObjects.welcomeMessage).toHaveText('Welcome Back, Malenia')
    })

    it('change otp method to Email', async () => {
        await pageObjects.settingIcon.waitForClickable({timeout: 10000})
        await pageObjects.settingIcon.click()
        await pageObjects.securityMenu.waitForClickable({timeout: 10000})
        await pageObjects.securityMenu.click()
        await pageObjects.otpDropdown.waitForClickable({timeout: 10000})
        await pageObjects.otpDropdown.click()
        const selectedMethod = await pageObjects.viaEmail.isClickable()

        if (selectedMethod === false) {
            await pageObjects.viaSms.click()
            await expect(pageObjects.otpDropdown).toHaveText('Send OTP via SMS')
            await pageObjects.otpDropdown.waitForClickable({timeout: 10000})
            await pageObjects.otpDropdown.click()
            await pageObjects.viaEmail.click()
          } 
          else {
            await pageObjects.viaEmail.click()
          }
        await expect(pageObjects.otpDropdown).toHaveText('Send OTP via Email')
    })
})
