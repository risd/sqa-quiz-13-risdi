class pageObjects {

    get inputUsername () {
        return $('[name="user[privyId]"]');
    }

    get inputPassword () {
        return $('[name="user[secret]"]');
    }

    get errorMessage () {
        return $('.input-error')
    }

    get welcomeMessage () {
        return $('.h-600.mb-3')
    }

    get settingIcon () {
        return $('.btn.btn-light.icon-only.mb-4.border-0.sidebar-setting-nav')
    }

    get securityMenu () {
        return $('body > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(6) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(2)')
    }

    get otpDropdown () {
        return $('#v-security-0__BV_toggle_ > span:nth-child(2)') 
    }

    get viaQrCode () {
        return $('#v-security-0 > ul > li:nth-child(1) > a')
    }

    get viawhatsApp () {
        return $('#v-security-0 > ul > li:nth-child(2) > a')
    }

    get viaEmail () {
        return $('(//a[normalize-space()="Send OTP via Email"])[1]')
    }

    get viaSms () {
        return $('(//a[normalize-space()="Send OTP via SMS"])[1]')
    }

    async validLogin (username, password) {
        await this.inputUsername.setValue(username)
        await browser.keys("Enter")
        await this.inputPassword.setValue(password)
        await browser.keys("Enter")
    }

    open () {
        return browser.url(`https://stg-app.privy.id`)
    }
}

module.exports = new pageObjects();